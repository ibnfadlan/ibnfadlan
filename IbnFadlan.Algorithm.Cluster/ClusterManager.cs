﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IbnFadlan.Algorithm.Cluster.Enums;
using IbnFadlan.Algorithm.Cluster.Helpers;
using IbnFadlan.Algorithm.Cluster.Models;

namespace IbnFadlan.Algorithm.Cluster
{
    // This class implements a agglomerative clustering algorithm based on the description of the ClusterManager algorithm (AGglomerative NEsting - kaufman rousseeuw 1990 - Finding groups in data)
    public class ClusterManager
    {
        #region private members

        private readonly PatternMatrix _patternMatrix;
        private readonly int _patternIndex;
        private readonly Clusters _clusters; // data structure for clustering
        private DissimilarityMatrix _dissimilarityMatrix;

        #endregion

        #region class constructors

        // dataset has no class attribute
        public ClusterManager(IEnumerable<double[]> dataSet)
        {
            _clusters = new Clusters();
            _patternMatrix = new PatternMatrix();

            foreach (var item in dataSet)
            {
                var pattern = new Pattern {Id = _patternIndex};
                pattern.AddAttributes(item);
                _patternMatrix.AddPattern(pattern);
                _patternIndex++;
            }
        }

        public ClusterManager(Dictionary<string, double[]> dataSet)
        {
            _clusters = new Clusters();
            _patternMatrix = new PatternMatrix();

            foreach (var item in dataSet)
            {
                var pattern = new Pattern
                {
                    Id = _patternIndex,
                    Name = item.Key
                };
                pattern.AddAttributes(item.Value);
                _patternMatrix.AddPattern(pattern);
                _patternIndex++;
            }
        }

        #endregion

        #region core algorithm functions

        #region Build dissimilarity matrix using .NET parallelism resources

        // compute the distance between all pair of clusters and store it on the dissimilarity matrix. this algorithm step is done using parallelization to improve performance.
        private void BuildDissimilarityMatrixParallel()
        {
            double distanceBetweenTwoClusters;
            _dissimilarityMatrix = new DissimilarityMatrix();

            Parallel.ForEach(ClusterPairCollection(), clusterPair =>
            {
                distanceBetweenTwoClusters =
                    ClusterDistance.ComputeDistance(clusterPair.Cluster1, clusterPair.Cluster2);
                _dissimilarityMatrix.AddClusterPairAndDistance(clusterPair, distanceBetweenTwoClusters);
            });
        }

        private IEnumerable<ClusterPair> ClusterPairCollection()
        {
            for (var i = 0; i < _clusters.Count(); i++)
            for (var j = i + 1; j < _clusters.Count(); j++)
            {
                var clusterPair = new ClusterPair
                {
                    Cluster1 = _clusters.GetCluster(i),
                    Cluster2 = _clusters.GetCluster(j)
                };

                yield return clusterPair;
            }
        }

        #endregion

        //initially, each pattern form a cluster
        private void BuildSingletonCluster()
        {
            _clusters.BuildSingletonCluster(_patternMatrix);
        }

        // update dissimilarity matrix with the distance of the new formed cluster
        private void UpdateDissimilarityMatrix(Models.Cluster newCluster, Strategy strategie)
        {
            for (var i = 0; i < _clusters.Count(); i++)
            {
                // compute the distance between old clusters to the new cluster
                var distanceBetweenClusters = ClusterDistance.ComputeDistance(_clusters.GetCluster(i), newCluster,
                    _dissimilarityMatrix, strategie);
                // insert the new cluster's distance
                _dissimilarityMatrix.AddClusterPairAndDistance(new ClusterPair(newCluster, _clusters.GetCluster(i)),
                    distanceBetweenClusters);
                //remove all old distance values of the old clusters (subclusters of the newcluster)
                _dissimilarityMatrix.RemoveClusterPair(
                    new ClusterPair(newCluster.GetSubCluster(0), _clusters.GetCluster(i)));
                _dissimilarityMatrix.RemoveClusterPair(
                    new ClusterPair(newCluster.GetSubCluster(1), _clusters.GetCluster(i)));
            }

            // finally, remove the distance of the old cluster pair
            _dissimilarityMatrix.RemoveClusterPair(
                new ClusterPair(newCluster.GetSubCluster(0), newCluster.GetSubCluster(1)));
        }

        private ClusterPair GetClosestClusterPairInDissimilarityMatrix()
        {
            return _dissimilarityMatrix.GetClosestClusterPair();
        }

        private void BuildHierarchicalClustering(int indexNewCluster, Strategy strategy, int k)
        {
            var closestClusterPair = GetClosestClusterPairInDissimilarityMatrix();

            // create a new cluster by merge the closest cluster pair
            var newCluster = new Models.Cluster();
            newCluster.AddSubCluster(closestClusterPair.Cluster1);
            newCluster.AddSubCluster(closestClusterPair.Cluster2);
            newCluster.Id = indexNewCluster;
            newCluster
                .UpdateTotalQuantityOfPatterns(); //update the total quantity of patterns of the new cluster (this quantity is used by UPGMA clustering strategy)

            //remove the closest cluster pair from the clustering data structure (clusters)
            _clusters.RemoveClusterPair(closestClusterPair);
            UpdateDissimilarityMatrix(newCluster, strategy);
            //add the new cluster to clustering
            _clusters.AddCluster(newCluster);

            // recursive call of this method while there is more than 1 cluster (k>2) in the clustering
            if (_clusters.Count() > k)
                BuildHierarchicalClustering(indexNewCluster + 1, strategy, k);
        }

        public Clusters ExecuteClustering(Strategy strategy, int k)
        {
            // Step 1
            // build a clustering only with singleton clusters
            BuildSingletonCluster();
            //build the dissimilarity matrix
            BuildDissimilarityMatrixParallel();

            // Step 3
            // build the hierarchical clustering 
            BuildHierarchicalClustering(_clusters.Count(), strategy, k);

            return _clusters; //represents the clustering data structure
        }

        // this method transform a hierarchical clustering into a partional clustering with k clusters. (this is necessary if we want to compare AGNES and K-Means results)
        public Models.Cluster[] BuildFlatClustersFromHierarchicalClustering(Clusters clusters, int k)
        {
            var flatClusters = new Models.Cluster[k];
            for (var i = 0; i < k; i++)
            {
                flatClusters[i] = new Models.Cluster {Id = i};
                foreach (var pattern in clusters.GetCluster(i).GetAllPatterns())
                    flatClusters[i].AddPattern(pattern);
            }

            return flatClusters;
        }

        #endregion
    }
}