﻿using System;
using IbnFadlan.Algorithm.Cluster.Enums;
using IbnFadlan.Algorithm.Cluster.Metrics;
using IbnFadlan.Algorithm.Cluster.Models;

namespace IbnFadlan.Algorithm.Cluster.Helpers
{
    public static class ClusterDistance
    {
        // this method compute distance between 2 singleton clusters
        public static double ComputeDistance(Models.Cluster cluster1, Models.Cluster cluster2)
        {
            double distance = 0;

            // if singleton cluster, then compute distance between patterns
            if (cluster1.QuantityOfPatterns() == 1 && cluster2.QuantityOfPatterns() == 1)
                distance = ComputePatternDistance(cluster1.GetPattern(0), cluster2.GetPattern(0));

            return distance;
        }

        // this method compute distance between clusters thas has subclusters (cluster2 represents the new cluster)
        public static double ComputeDistance(Models.Cluster cluster1, Models.Cluster cluster2,
            DissimilarityMatrix dissimilarityMatrix, Strategy strategy)
        {
            double distance;
            //get the distance between cluster1 and subcluster0 of the cluster2
            var distance1 =
                dissimilarityMatrix.ReturnClusterPairDistance(new ClusterPair(cluster1, cluster2.GetSubCluster(0)));
            //get the distance between cluster1 and subcluster1 of the cluster cluster2
            var distance2 =
                dissimilarityMatrix.ReturnClusterPairDistance(new ClusterPair(cluster1, cluster2.GetSubCluster(1)));

            switch (strategy)
            {
                case Strategy.AverageLinkageWpgma:
                    distance = (distance1 + distance2) / 2;
                    break;
                case Strategy.AverageLinkageUpgma:
                    distance = cluster2.GetSubCluster(0).TotalQuantityOfPatterns * distance1 /
                               cluster2.TotalQuantityOfPatterns + cluster2.GetSubCluster(1).TotalQuantityOfPatterns *
                               distance2 / cluster2.TotalQuantityOfPatterns;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(strategy), strategy, null);
            }

            return distance;
        }

        private static double ComputePatternDistance(Pattern pattern1, Pattern pattern2)
        {
            return Distance.GetEuclidianDistance(pattern1.GetAttributes(), pattern2.GetAttributes());
        }
    }
}