﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IbnFadlan.Algorithm.Cluster.Models
{
    public class PatternMatrix : IEnumerable
    {
        private readonly HashSet<Pattern> _patternCollection;

        public PatternMatrix()
        {
            _patternCollection = new HashSet<Pattern>();
        }


        public IEnumerator GetEnumerator()
        {
            return _patternCollection.GetEnumerator();
        }

        public void AddPattern(Pattern pattern)
        {
            _patternCollection.Add(pattern);
        }


        public Pattern[] GetPatterns()
        {
            return _patternCollection.ToArray();
        }

        public Pattern GetPattern(int index)
        {
            return _patternCollection.ElementAt(index);
        }

        public int Size()
        {
            return _patternCollection.Count;
        }
    }
}