﻿using System;

namespace IbnFadlan.Algorithm.Cluster.Models
{
    //This class stores the pairs of cluster's id which is the dissimilarity matrix entry
    public class ClusterPair
    {
        #region constructors

        public ClusterPair()
        {
        }

        public ClusterPair(Cluster cluster1, Cluster cluster2)
        {
            Cluster1 = cluster1 ?? throw new ArgumentNullException("cluster1");
            Cluster2 = cluster2 ?? throw new ArgumentNullException("cluster2");
        }

        #endregion

        #region class properties

        public Cluster Cluster1 { get; set; }

        public Cluster Cluster2 { get; set; }

        #endregion
    }
}