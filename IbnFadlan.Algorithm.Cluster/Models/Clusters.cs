﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IbnFadlan.Algorithm.Cluster.Models
{
    // clustering data structure 
    public class Clusters : IEnumerable
    {
        private readonly HashSet<Cluster> _clusters;

        public Clusters()
        {
            _clusters = new HashSet<Cluster>();
        }

        public int Id { get; private set; }

        public IEnumerator GetEnumerator()
        {
            return _clusters.GetEnumerator();
        }


        public void AddCluster(Cluster cluster)
        {
            _clusters.Add(cluster);
        }

        public void RemoveCluster(Cluster cluster)
        {
            _clusters.Remove(cluster);
        }

        public Cluster GetCluster(int index)
        {
            return _clusters.ElementAt(index);
        }

        public Cluster[] GetClusters()
        {
            return _clusters.ToArray();
        }

        public int Count()
        {
            return _clusters.Count;
        }

        //add a single pattern to a cluster 
        public void BuildSingletonCluster(PatternMatrix patternMatrix)
        {
            var clusterId = 0;

            foreach (Pattern item in patternMatrix)
            {
                var cluster = new Cluster {Id = clusterId};
                cluster.AddPattern(item);
                cluster.TotalQuantityOfPatterns = 1;
                _clusters.Add(cluster);
                clusterId++;
            }
        }

        //remove a cluster pair from the clustering data structure
        public void RemoveClusterPair(ClusterPair clusterPair)
        {
            RemoveCluster(clusterPair.Cluster1);
            RemoveCluster(clusterPair.Cluster2);
        }
    }
}