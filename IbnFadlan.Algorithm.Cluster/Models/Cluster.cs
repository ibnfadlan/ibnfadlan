﻿using System.Collections.Generic;
using System.Linq;

namespace IbnFadlan.Algorithm.Cluster.Models
{
    public class Cluster
    {
        #region class constructor

        public Cluster()
        {
            _cluster = new HashSet<Pattern>();
            _subClusters = new HashSet<Cluster>();
        }

        #endregion

        #region private members

        private readonly HashSet<Pattern> _cluster; // singleton cluster formed by one pattern
        private readonly HashSet<Cluster> _subClusters; // child clusters
        private List<Pattern> _patternList; // list with all cluster's patterns and its subclusters

        #endregion

        #region class properties

        public int Id { get; set; }

        public int TotalQuantityOfPatterns { get; set; }


        public Pattern Centroid { get; set; }

        #endregion

        #region class methods

        public void AddPattern(Pattern pattern)
        {
            _cluster.Add(pattern);
        }

        public int QuantityOfPatterns()
        {
            return _cluster.Count;
        }

        public Pattern[] GetPatterns()
        {
            return _cluster.ToArray();
        }

        public Pattern GetPattern(int index)
        {
            return _cluster.ElementAt(index);
        }

        public void AddSubCluster(Cluster subCluster)
        {
            _subClusters.Add(subCluster);
        }

        public Cluster[] GetSubClusters()
        {
            return _subClusters.ToArray();
        }

        public int QuantityOfSubClusters()
        {
            return _subClusters.Count;
        }

        public Cluster GetSubCluster(int index)
        {
            return _subClusters.ElementAt(index);
        }

        public int UpdateTotalQuantityOfPatterns()
        {
            //if cluster has subclustes, then calculate how many patterns there is in each subcluster
            if (!GetSubClusters().Any()) return TotalQuantityOfPatterns;
            TotalQuantityOfPatterns = 0;
            foreach (var subcluster in GetSubClusters())
            {
                if (subcluster != null)
                {
                    TotalQuantityOfPatterns = TotalQuantityOfPatterns + subcluster.UpdateTotalQuantityOfPatterns();
                }
            }
            // if there is no subcluster, it is because is a singleton cluster (i.e., totalNumberOfPatterns = 1)
            return TotalQuantityOfPatterns;
        }

        public void ClearPatterns()
        {
            _cluster.Clear();
        }

        public List<Pattern> GetAllPatterns()
        {
            _patternList = new List<Pattern>();
            if (QuantityOfSubClusters() == 0)
                foreach (var pattern in GetPatterns())
                    _patternList.Add(pattern);
            else
                foreach (var subCluster in GetSubClusters())
                    _GetSubClusterPattern(subCluster);

            return _patternList;
        }

        private void _GetSubClusterPattern(Cluster subCluster)
        {
            if (subCluster.QuantityOfSubClusters() == 0)
                foreach (var pattern in subCluster.GetPatterns())
                    _patternList.Add(pattern);
            else
                foreach (var _subCluster in subCluster.GetSubClusters())
                {
                    _GetSubClusterPattern(_subCluster);
                }
        }

        #endregion
    }
}