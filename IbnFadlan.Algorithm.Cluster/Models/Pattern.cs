﻿using System.Collections.Generic;
using System.Linq;

namespace IbnFadlan.Algorithm.Cluster.Models
{
    //data structure that stores data set numerical values
    public class Pattern
    {
        #region class constructor

        public Pattern()
        {
            _attributeCollection = new List<double>();
        }

        #endregion

        public string Name { get; set; }

        #region private members

        private readonly List<double> _attributeCollection; //attribute collection of a pattern

        #endregion

        #region class properties

        public int Id { get; set; }

        public int ClassAttribute { get; set; }

        #endregion

        #region class methods

        public void AddAttribute(double attribute)
        {
            _attributeCollection.Add(attribute);
        }

        public void RemoveAttributeAt(int i)
        {
            _attributeCollection.RemoveAt(i);
        }

        public double GetAttribute(int index)
        {
            return _attributeCollection[index];
        }

        public void AddAttributes(double[] attributes)
        {
            _attributeCollection.AddRange(attributes);
        }

        public double[] GetAttributes()
        {
            return _attributeCollection.ToArray<double>();
        }

        public int GetDimension()
        {
            return _attributeCollection.Count;
        }

        #endregion
    }
}