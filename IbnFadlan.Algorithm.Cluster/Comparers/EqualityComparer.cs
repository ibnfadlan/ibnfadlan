﻿using System.Collections.Generic;
using IbnFadlan.Algorithm.Cluster.Models;

namespace IbnFadlan.Algorithm.Cluster.Comparers
{
    public class EqualityComparer : IEqualityComparer<ClusterPair>
    {
        //see IEqualyComparer_Example in ProgrammingTips folder for better understanding of this concept
        //the implementation of the IEqualityComparer is necessary because ClusterPair has two keys (cluster1.Id and cluster2.Id in ClusterPair) to compare
        public bool Equals(ClusterPair x, ClusterPair y)
        {
            return y != null && x != null && x.Cluster1.Id == y.Cluster1.Id && x.Cluster2.Id == y.Cluster2.Id;
        }

        public int GetHashCode(ClusterPair x)
        {
            return x.Cluster1.Id ^ x.Cluster2.Id;
        }
    }

}