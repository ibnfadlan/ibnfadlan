﻿namespace IbnFadlan.Algorithm.Cluster.Enums
{
    public enum Strategy
    {
        AverageLinkageWpgma,
        AverageLinkageUpgma
    }
}