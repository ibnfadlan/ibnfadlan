﻿using System;
using System.Linq;
using IbnFadlan.Algorithm.Cluster.Models;

namespace IbnFadlan.Algorithm.Cluster.Metrics
{
    public static class Distance
    {
        public static double GetEuclidianDistance(double[] x, double[] y)
        {
            if (x.Length != y.Length)
                throw new ArgumentException("Input vectors must be of the same dimension.");

            var distance = x.Select((t, i) => t - y[i]).Sum(diff => diff * diff);

            return Math.Sqrt(distance);

        }

        public static double CalculatePatternDistance(Pattern pattern1, Pattern pattern2)
        {
            return GetEuclidianDistance(pattern1.GetAttributes(), pattern2.GetAttributes());
        }

    }
}
