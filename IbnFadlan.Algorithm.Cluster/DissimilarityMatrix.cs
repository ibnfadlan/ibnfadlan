﻿using System.Collections.Concurrent;
using System.Linq;
using IbnFadlan.Algorithm.Cluster.Comparers;
using IbnFadlan.Algorithm.Cluster.Models;

namespace IbnFadlan.Algorithm.Cluster
{
    public class DissimilarityMatrix
    {
        // list to store distance values from a pair of clusters Dictionary<ClusterPair, Distance>
        private readonly ConcurrentDictionary<ClusterPair, double> _distanceMatrix;

        public DissimilarityMatrix()
        {
            _distanceMatrix = new ConcurrentDictionary<ClusterPair, double>(new EqualityComparer());
        }

        public void AddClusterPairAndDistance(ClusterPair clusterPair, double distance)
        {
            _distanceMatrix.TryAdd(clusterPair, distance);
        }

        public void RemoveClusterPair(ClusterPair clusterPair)
        {
            double outvalue;

            if (_distanceMatrix.ContainsKey(clusterPair))
                _distanceMatrix.TryRemove(clusterPair, out outvalue);
            else
                _distanceMatrix.TryRemove(new ClusterPair(clusterPair.Cluster2, clusterPair.Cluster1), out outvalue);
        }

        // get the lowest distance in distance matrix
        public double GetLowestDistance()
        {
            return _distanceMatrix.Select(item => item.Value).Concat(new[] {double.MaxValue}).Min();
        }

        public double ReturnLowestDistanceOld()
        {
            var distanceList = _distanceMatrix.ToList();
            distanceList.Sort(
                (x, y) => x.Value
                    .CompareTo(y
                        .Value)); // it is necessary to find a more performatic way to find this value (very important)
            return distanceList[0].Value;
        }


        // get the closest cluster pair (i.e., min cluster pair distance). it is also important to reduce computational time
        public ClusterPair GetClosestClusterPair()
        {
            var minDistance = double.MaxValue;
            var closestClusterPair = new ClusterPair();

            foreach (var item in _distanceMatrix)
            {
                if (!(item.Value < minDistance)) continue;
                minDistance = item.Value;
                closestClusterPair = item.Key;
            }

            return closestClusterPair;
        }


        // get the distance value from a cluster pair. THIS METHOD DEPENDS ON THE EqualityComparer IMPLEMENTATION IN ClusterPair CLASS
        public double ReturnClusterPairDistance(ClusterPair clusterPair)
        {
            // look in distance matrix if there is an input of cluster1 and cluster2 (remember that ClusterPair has two childs cluster1 and cluster2)
            var clusterPairDistance = _distanceMatrix.ContainsKey(clusterPair)
                ? _distanceMatrix[clusterPair]
                : _distanceMatrix[new ClusterPair(clusterPair.Cluster2, clusterPair.Cluster1)];

            return clusterPairDistance;
        }
    }
}