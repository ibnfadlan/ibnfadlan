﻿using System.Linq;
using IbnFadlan.Crawler.Crawlers;
using IbnFadlan.Data.Repository.Implemetation;

namespace IbnFadlan.Crawler.App
{
    public class Program
    {
        static void Main(string[] args)
        {
            var crawler = new UnCrawler();
            var data = crawler.GetMigrationDataAsync().Result;

            var unitOfWork = new MigrationDataUnitOfWork();
            
            var totalCount = data.Count();
            var index = 0;

            foreach (var element in data)
            {
                index++;
                if (element.Value == 0)
                {
                    continue;
                }
                unitOfWork.SaveMigration(element);
            }
        }
    }
}
