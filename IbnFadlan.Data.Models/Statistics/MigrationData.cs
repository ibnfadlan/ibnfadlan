﻿using System;
using IbnFadlan.Data.Models.Locations;

namespace IbnFadlan.Data.Models.Statistics
{
    public class MigrationData
    {
        public Guid Id { get; set; }

        public Country From { get; set; }

        public Country To { get; set; }

        public DateTimeOffset Date { get; set; }

        public float Value { get; set; }
    }
}