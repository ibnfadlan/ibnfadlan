﻿using System;
using System.Collections.Generic;
using IbnFadlan.Data.Models.Locations;

namespace IbnFadlan.Data.Models
{
    public class Tag
    {
        public Guid TagId { get; set; }

        public string Name { get; set; }

        public IList<University> Universities { get; set; }

        public IList<City> Cities { get; set; }

        public IList<Attraction> Attractions { get; set; }

        public IList<Airport> Airports { get; set; }

        public IList<Country> Countries { get; set; }

        public IList<ApplicationUser> ApplicationUsers { get; set; }
    }
}