﻿namespace IbnFadlan.Data.Models.Documents
{
    public enum VisaType
    {
        Travel,
        Work
    }
}