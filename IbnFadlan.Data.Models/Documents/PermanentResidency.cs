﻿using System;

namespace IbnFadlan.Data.Models.Documents
{
    public class PermanentResidency
    {
        public Guid Id { get; set; }

        public string Limitations { get; set; }

        public string Obligations { get; set; }

        public string Description { get; set; }
    }
}