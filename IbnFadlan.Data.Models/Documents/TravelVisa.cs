﻿using System;

namespace IbnFadlan.Data.Models.Documents
{
    public class TravelVisa
    {
        public Guid Id { get; set; }

        public VisaType Type { get; set; }

        public int Duration { get; set; }

        public string Description { get; set; }
    }
}