﻿using System;

namespace IbnFadlan.Data.Models.Locations
{
    public class Airport : PlaceBase
    {
        public Guid Id { get; set; }

        public string Iata { get; set; }

        public string Icao { get; set; }

        public string Wmo { get; set; }

        public string Operation { get; set; }
    }
}