﻿using System;

namespace IbnFadlan.Data.Models.Locations
{
    public class Attraction : PlaceBase
    {
        public Guid Id { get; set; }

        public bool PublicTransitAccess { get; set; }

        public string Address { get; set; }
    }
}