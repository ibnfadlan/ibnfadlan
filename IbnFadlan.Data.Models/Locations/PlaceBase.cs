﻿using System.Collections.Generic;

namespace IbnFadlan.Data.Models.Locations
{
    public class PlaceBase
    {
        public string Name { get; set; }

        public string Website { get; set; }

        public string Coordinates { get; set; }

        public string Descriptions { get; set; }

        public IList<Tag> Tags { get; set; }
    }
}