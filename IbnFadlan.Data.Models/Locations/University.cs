﻿using System;

namespace IbnFadlan.Data.Models.Locations
{
    public class University : PlaceBase
    {
        public Guid Id { get; set; }

        public string Address { get; set; }
    }
}