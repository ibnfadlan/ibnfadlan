﻿using System;
using System.Collections.Generic;
using IbnFadlan.Data.Models.Documents;

namespace IbnFadlan.Data.Models.Locations
{
    public class Country
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Currency { get; set; }

        public string TimeZone { get; set; }

        public string DrivesOnThe { get; set; }

        public string CallingCode { get; set; }

        public string OfficialLanguage { get; set; }

        public string Website { get; set; }

        public string Description { get; set; }

        public int Area { get; set; }

        public int Population { get; set; }

        public IList<Tag> Tags { get; set; }

        public IList<PermanentResidency> PermanentResidencies { get; set; }

        public IList<TravelVisa> TravelVisas { get; set; }

        public IList<City> Cities { get; set; }
    }
}