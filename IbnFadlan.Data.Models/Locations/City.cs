﻿using System;
using System.Collections.Generic;

namespace IbnFadlan.Data.Models.Locations
{
    public class City : PlaceBase
    {
        public Guid Id { get; set; }

        public bool IsCapital { get; set; }

        public int Population { get; set; }

        public int Area { get; set; }

        public string PostalCode { get; set; }

        public Region Region { get; set; }
              
        public IList<University> Universities { get; set; }
                
        public IList<Attraction> Attractions { get; set; }

        public IList<Airport> Airports { get; set; }
    }
}