﻿using System;
using System.Collections.Generic;
using System.Linq;
using IbnFadlan.Business.Services;
using IbnFadlan.Business.Services.Enums;

namespace IbnFadlan.Business
{
    public class StatisticService
    {
        private readonly MigrationService _migrationService;

        public StatisticService()
        {
            _migrationService = new MigrationService();
        }

        public IList<List<string>> GetClusteredData(string countryName, 
            Direction direction, int? year = null)
        {
            var data = _migrationService.GetClusteredDataForCountry(countryName, direction);

            if (year.HasValue)
            {
                data = data.Where(x => x.Date.Year == year.Value).ToList();
            }

            var group = direction == Direction.From
                ? data.GroupBy(x => x.To.Name)
                : data.GroupBy(x => x.From.Name);

            return group.Select(x =>
                    new List<string>
                    {
                        x.Key,
                        x.Sum(y => y.Value).ToString(),
                        x.FirstOrDefault().ClusterId.ToString()
                    })
                .ToList(); ;
        }

        public IList<List<string>> GetData(string countryName, Direction direction,
            int? year = null)
        {
            var data = _migrationService.GetAllDataForCountry(countryName, direction);

            if (year.HasValue)
            {
                data = data.Where(x => x.Date.Year == year.Value).ToList();
            }

            var group = direction == Direction.From
                ? data.GroupBy(x => x.To.Name)
                : data.GroupBy(x => x.From.Name);
            
            return group.Select(x => new List<string>
                {
                    x.Key,
                    x.Sum(y => y.Value).ToString()
                })
                .ToList();
        }

        public IDictionary<int, int> GetFullData(string countryName, Direction direction)
        {
            var result = new Dictionary<int, int>();
            var data = _migrationService.GetAllDataForCountry(countryName, direction);
                
            var dates = data.OrderBy(x => x.Date.Year).GroupBy(x => x.Date.Year);

            foreach (var date in dates)
            {
                result.Add(date.Key, date.Sum(x => x.Value));
            }
            
            return result;
        }

        public struct ClusterItem
        {
            public int Year { get; set; }
            public int Value { get; set; }
            public int Cluster { get; set; }

        }
    }
}