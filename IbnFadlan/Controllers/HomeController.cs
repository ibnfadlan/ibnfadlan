﻿using System.Web.Mvc;
using IbnFadlan.Business.Services.Enums;

namespace IbnFadlan.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Charts(string country, Direction direction, int? year)
        {
            return View(new ChartSetViewModel(country, direction, year));
        }

        public ActionResult Bf()
        {
            return RedirectToAction("Charts", new { country = "Belarus", direction = Direction.From});
        }

        public ActionResult Bt()
        {
            return RedirectToAction("Charts", new { country = "Belarus", direction = Direction.To });
        }

        public ActionResult TrendLines()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }

    public class ChartSetViewModel
    {
        public ChartSetViewModel(string country, Direction direction, int? year)
        {
            Country = country;
            Direction = direction;
            Year = year;
        }

        public string Country { get; set; }
        public Direction Direction { get; set; }
        public int? Year { get; set; }
    }
}