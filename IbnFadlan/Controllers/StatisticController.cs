﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IbnFadlan.Algorithm.Search;
using IbnFadlan.Algorithm.Trend;
using IbnFadlan.Business;
using IbnFadlan.Business.Services.Enums;
using IbnFadlan.Models;

namespace IbnFadlan.Controllers
{
    public class StatisticController : Controller
    {
        private readonly StatisticService _statisticService;

        public StatisticController()
        {
            _statisticService = new StatisticService();
        }

        public ActionResult PieChart(string country, Direction direction, int? year)
        {
            var data = _statisticService.GetData(country, direction, year);

            return View(data);
        }

        public ActionResult Map(string country, Direction direction, int? year)
        {
            var data = _statisticService.GetClusteredData(country, direction, year);

            return View(data);
        }

        public ActionResult MapClusters(string country, Direction direction, int? year)
        {
            var data = _statisticService.GetClusteredData(country, direction, year);

            return View(data);
        }

        public ActionResult TreeMap(string country, Direction direction, int? year)
        {
            var data = _statisticService.GetClusteredData(country, direction, year);

            return View(data);
        }

        public ActionResult TrendLine(string country)
        {
            var from = _statisticService.GetFullData(country, Direction.From);
            var to = _statisticService.GetFullData(country, Direction.To);

            var years = from.Keys.Union(to.Keys).Distinct().ToList();

            var fromValues = from.Values.ToList();
            var toValues = to.Values.ToList();

            var trendLineFrom = new TrendLineLinear(years.Select(x => (double) x),
                fromValues.Select(x => (double) x));
            
            var additionalYears = new List<int>
            {
                2020,
                2025,
                2030
            };
            
            var trendLineTo = new TrendLineLinear(years.Select(x => (double)x),
                toValues.Select(x => (double)x));

            fromValues
                .AddRange(trendLineFrom.GetGrowth(additionalYears.Select(x => (double)x).ToList())
                .Select(x => (int)x));

            toValues
                .AddRange(trendLineTo.GetGrowth(additionalYears.Select(x => (double)x).ToList())
                    .Select(x => (int)x));

            years.AddRange(additionalYears);

            var vm = new TrendLineViewModel
            {
                CountryName = country,
                From = fromValues,
                To = toValues,
                Years = years
            };
            
            return View(vm);
        }
    }
}