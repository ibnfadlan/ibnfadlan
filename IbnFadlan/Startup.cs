﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IbnFadlan.Startup))]
namespace IbnFadlan
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
