﻿using System;
using System.Collections.Generic;
using IbnFadlan.Business.Services.Enums;

namespace IbnFadlan.Models
{
    public class TrendLineViewModel
    {
        public string CountryName { get; set; }

        public IList<int> From { get; set; }

        public IList<int> To { get; set; }

        public IList<int> Years { get; set; }

        public List<List<int>> Clusters { get; set; }
    }
}