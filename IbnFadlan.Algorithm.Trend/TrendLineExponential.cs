﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IbnFadlan.Algorithm.Trend
{
    public class TrendLineExponential
    {
        public IEnumerable<double> GetGrowth(IList<double> yValues, IList<double> xValues, 
            IList<double> xValuesToCalculate, bool useConst = false)
        {
            if (yValues == null)
            {
                return null;
            }

            if (xValues == null)
            {
                xValues = new List<double>();
                for (var i = 0; i <= yValues.Count; i++)
                {
                    xValues.Add(i++);
                }
            }

            if (xValuesToCalculate == null)
            {
                xValuesToCalculate = new List<double>();
                for (var i = 0; i <= yValues.Count; i++)
                {
                    xValuesToCalculate.Add(i++);
                }
            }

            var n = yValues.Count;
            var avgX = 0.0;
            var avgY = 0.0;
            var avgXy = 0.0;
            var avgXx = 0.0;
            double beta;
            double alpha;

            for (var i = 0; i < n; i++)
            {
                var x = xValues[i];
                var y = Math.Log(yValues[i]);
                avgX += x;
                avgY += y;
                avgXy += x * y;
                avgXx += x * x;
            }

            avgX /= n;
            avgY /= n;
            avgXy /= n;
            avgXx /= n;
            
            if (useConst)
            {
                beta = (avgXy - avgX * avgY) / (avgXx - avgX * avgX);
                alpha = avgY - beta * avgX;
            }
            else
            {
                beta = avgXy / avgXx;
                alpha = 0.0;
            }
            
            return xValuesToCalculate.Select(x => Math.Exp(alpha + beta * x)).ToList();
        }
    }
}