﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IbnFadlan.Algorithm.Trend
{
    public class TrendLineLinear
    {
        private readonly double _n;

        private readonly double _sumX;

        private readonly double _sumX2;

        private readonly double _sumXy;

        private readonly double _sumY;

        public TrendLineLinear(IEnumerable<double> x, IEnumerable<double> y)
        {
            ValuesX = x.ToArray();
            ValuesY = y.ToArray();

            _sumXy = CalculateSumXsYsProduct(ValuesX, ValuesY);
            _sumX = CalculateSumXs(ValuesX);
            _sumY = CalculateSumYs(ValuesY);
            _sumX2 = CalculateSumXsSquare(ValuesX);
            _n = ValuesX.Length;

            CalculateSlope();
            CalculateOffset();
        }

        public IList<double> GetGrowth(IList<double> xValues)
        {
            return xValues.Select(x => x * Slope + Offset).ToList();
        }

        private double Slope { get; set; }

        private double Offset { get; set; }

        private double[] ValuesX { get; }

        private double[] ValuesY { get; }

        private double CalculateSumXsYsProduct(IReadOnlyList<double> xs, IReadOnlyList<double> ys)
        {
            return xs.Select((t, i) => t * ys[i]).Sum();
        }

        private double CalculateSumXs(IEnumerable<double> xs)
        {
            return xs.Sum();
        }

        private double CalculateSumYs(IEnumerable<double> ys)
        {
            return ys.Sum();
        }

        private double CalculateSumXsSquare(IEnumerable<double> xs)
        {
            return xs.Sum(x => Math.Pow(x, 2));
        }

        private void CalculateSlope()
        {
            try
            {
                Slope = (_n * _sumXy - _sumX * _sumY) / (_n * _sumX2 - Math.Pow(_sumX, 2));
            }
            catch (DivideByZeroException)
            {
                Slope = 0;
            }
        }

        private void CalculateOffset()
        {
            try
            {
                Offset = (_sumY - Slope * _sumX) / _n;
            }
            catch (DivideByZeroException)
            {
                Offset = 0;
            }
        }
    }
}