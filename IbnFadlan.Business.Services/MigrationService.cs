﻿using System;
using System.Collections.Generic;
using System.Linq;
using IbnFadlan.Algorithm.Cluster;
using IbnFadlan.Algorithm.Cluster.Enums;
using IbnFadlan.Business.Services.Enums;
using IbnFadlan.Business.Services.Models;
using IbnFadlan.Core.Extensions;
using IbnFadlan.Data.Models.Statistics;
using IbnFadlan.Data.Repository.Implemetation;
using IbnFadlan.Data.Repository.Interface;

namespace IbnFadlan.Business.Services
{
    public class MigrationService
    {
        private readonly IMigationDataUnitOfWork _migationDataUnitOfWork;

        public MigrationService()
        {
            _migationDataUnitOfWork = new MigrationDataUnitOfWork();
        }

        public IList<MigrationItem> GetAllDataForCountry(string countryName, Direction direction)
        {
            var countries = direction == Direction.From
                ? _migationDataUnitOfWork.GetAllDataByFromCountry(countryName)
                : _migationDataUnitOfWork.GetAllDataByToCountry(countryName);

            return countries.Select(GetMigrationItem).ToList();
        }

        private MigrationItem GetMigrationItem(MigrationData data)
        {
            return new MigrationItem
            {
                Date = data.Date,
                From = new CountryInfo
                {
                    Name = data.From.Name
                },
                To = new CountryInfo
                {
                    Name = data.To.Name
                },
                Value = Convert.ToInt32(data.Value)
            };
        }

        public IList<MigrationItem> GetClusteredDataForCountry(string countryName, Direction direction)
        {
            var migrationDatas = direction == Direction.From
                ? _migationDataUnitOfWork.GetAllDataByFromCountry(countryName)
                : _migationDataUnitOfWork.GetAllDataByToCountry(countryName);

            var countries = migrationDatas.Select(GetMigrationItem).ToList();

            var group = direction == Direction.From
                ? countries.GroupBy(x => x.To.Name)
                : countries.GroupBy(x => x.From.Name);

            var elements = new Dictionary<string, double[]>();

            foreach (var country in group)
                elements.Add(country.Key, country.Select(x => (double) x.Value).ToArray(20));

            var clusterManager = new ClusterManager(elements);

            const int numberOfClusters = 7;
            var clusters = clusterManager.ExecuteClustering(Strategy.AverageLinkageUpgma, numberOfClusters);

            foreach (var cluster in clusters.GetClusters())
            foreach (var patters in cluster.GetAllPatterns())
            {
                var country = direction == Direction.From
                    ? countries.FirstOrDefault(x => x.To.Name.Equals(patters.Name, StringComparison.OrdinalIgnoreCase))
                    : countries.FirstOrDefault(x => x.From.Name.Equals(patters.Name, StringComparison.OrdinalIgnoreCase));
                if (country != null)
                    country.ClusterId = cluster.Id;
            }

            return countries;
        }
    }
}