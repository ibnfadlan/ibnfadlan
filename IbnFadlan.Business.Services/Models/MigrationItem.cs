﻿using System;

namespace IbnFadlan.Business.Services.Models
{
    public class MigrationItem
    {
        public CountryInfo From { get; set; }

        public CountryInfo To { get; set; }

        public DateTimeOffset Date { get; set; }

        public int Value { get; set; }

        public int ClusterId { get; set; }
    }
}