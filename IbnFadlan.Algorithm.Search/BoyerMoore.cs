﻿using System;

namespace IbnFadlan.Algorithm.Search
{
    /// <summary>
    ///     Implements Boyer-Moore search algorithm
    /// </summary>
    public class BoyerMoore
    {
        // Returned index when no match found
        public const int InvalidIndex = -1;

        private bool _ignoreCase;
        private string _pattern;
        private UnicodeSkipArray _skipArray;

        public BoyerMoore(string pattern)
        {
            Initialize(pattern, false);
        }

        public BoyerMoore(string pattern, bool ignoreCase)
        {
            Initialize(pattern, ignoreCase);
        }

        /// <summary>
        ///     Initializes this instance to search a new pattern.
        /// </summary>
        /// <param name="pattern">Pattern to search for</param>
        public void Initialize(string pattern)
        {
            Initialize(pattern, false);
        }

        /// <summary>
        ///     Initializes this instance to search a new pattern.
        /// </summary>
        /// <param name="pattern">Pattern to search for</param>
        /// <param name="ignoreCase">If true, search is case-insensitive</param>
        public void Initialize(string pattern, bool ignoreCase)
        {
            _pattern = pattern;
            _ignoreCase = ignoreCase;

            // Create multi-stage skip table
            _skipArray = new UnicodeSkipArray(_pattern.Length);
            // Initialize skip table for this pattern
            if (_ignoreCase)
                for (var i = 0; i < _pattern.Length - 1; i++)
                {
                    _skipArray[char.ToLower(_pattern[i])] = (byte) (_pattern.Length - i - 1);
                    _skipArray[char.ToUpper(_pattern[i])] = (byte) (_pattern.Length - i - 1);
                }
            else
                for (var i = 0; i < _pattern.Length - 1; i++)
                    _skipArray[_pattern[i]] = (byte) (_pattern.Length - i - 1);
        }

        /// <summary>
        ///     Searches for the current pattern within the given text
        ///     starting at the beginning.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public int Search(string text)
        {
            return Search(text, 0);
        }

        /// <summary>
        ///     Searches for the current pattern within the given text
        ///     starting at the specified index.
        /// </summary>
        /// <param name="text">Text to search</param>
        /// <param name="startIndex">Offset to begin search</param>
        /// <returns></returns>
        public int Search(string text, int startIndex)
        {
            var i = startIndex;

            // Loop while there's still room for search term
            while (i <= text.Length - _pattern.Length)
            {
                // Look if we have a match at this position
                var j = _pattern.Length - 1;
                if (_ignoreCase)
                    while (j >= 0 && char.ToUpper(_pattern[j]) == char.ToUpper(text[i + j]))
                        j--;
                else
                    while (j >= 0 && _pattern[j] == text[i + j])
                        j--;

                if (j < 0)
                    return i;

                // Advance to next comparision
                i += Math.Max(_skipArray[text[i + j]] - _pattern.Length + 1 + j, 1);
            }
            // No match found
            return InvalidIndex;
        }
    }
}