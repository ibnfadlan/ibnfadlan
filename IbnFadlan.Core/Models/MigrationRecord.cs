﻿using System;

namespace IbnFadlan.Core.Models
{
    public class MigrationRecord
    {
        public string From { get; set; }
        public string To { get; set; }
        public DateTime Date { get; set; }
        public int Value { get; set; }
    }
}