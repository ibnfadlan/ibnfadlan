﻿using System;
using System.Collections.Generic;
using System.Linq;
using IbnFadlan.Core.Extensions;
using IbnFadlan.Core.Models;
using IbnFadlan.Data.Models.Locations;
using IbnFadlan.Data.Models.Statistics;
using IbnFadlan.Data.Repository.General;
using IbnFadlan.Data.Repository.Interface;

namespace IbnFadlan.Data.Repository.Implemetation
{
    public class MigrationDataUnitOfWork : UnitOfWorkBase, IMigationDataUnitOfWork
    {
        private GenericRepository<Country> _countryRepository;
        private GenericRepository<MigrationData> _migationDataRepository;
        
        public GenericRepository<Country> CountryRepository => _countryRepository ??
                                                               (_countryRepository =
                                                                   new GenericRepository<Country>(Context));

        public GenericRepository<MigrationData> MigrationDataRepository => _migationDataRepository ??
                                                                           (_migationDataRepository =
                                                                               new GenericRepository<MigrationData>(
                                                                                   Context));

        public void SaveMigration(Dictionary<string, object> element)
        {
            var fromName = (string) element["CountryFrom"];
            var toName = (string) element["CountryTo"];

            Country countryFrom;
            if (Context.Countries.Any(x => x.Name.Equals(fromName)))
            {
                countryFrom = Context.Countries.FirstOrDefault(x => x.Name.Equals(fromName));
            }
            else
            {
                countryFrom = new Country
                {
                    Id = Guid.NewGuid(),
                    Name = fromName
                };
                Context.Countries.Add(countryFrom);
                Context.SaveChanges();
            }

            Country countryTo;
            if (Context.Countries.Any(x => x.Name.Equals(toName)))
            {
                countryTo = Context.Countries.FirstOrDefault(x => x.Name.Equals(toName));
            }
            else
            {
                countryTo = new Country
                {
                    Id = Guid.NewGuid(),
                    Name = toName
                };
                Context.Countries.Add(countryTo);
                Context.SaveChanges();
            }

            var value = Convert.ToSingle(element["Value"]);
            DateTimeOffset.TryParse(element["Date"].ToString(), out DateTimeOffset date);

            var migationData = new MigrationData
            {
                Id = Guid.NewGuid(),
                From = countryFrom,
                To = countryTo,
                Value = value,
                Date = date
            };

            Context.MigrationDatas.Add(migationData);
            Context.SaveChanges();
        }
        
        public void SaveMigration(MigrationRecord element)
        {
            var fromName = element.From;
            var toName = element.To;
            var value = Convert.ToSingle(element.Value);
            var date = element.Date;
            
            Country countryFrom;
            if (Context.Countries.Any(x => x.Name.Equals(fromName)))
            {
                countryFrom = Context.Countries.FirstOrDefault(x => x.Name.Equals(fromName));
            }
            else
            {
                countryFrom = new Country
                {
                    Id = Guid.NewGuid(),
                    Name = fromName
                };
                Context.Countries.Add(countryFrom);
                Context.SaveChanges();
            }

            Country countryTo;
            if (Context.Countries.Any(x => x.Name.Equals(toName)))
            {
                countryTo = Context.Countries.FirstOrDefault(x => x.Name.Equals(toName));
            }
            else
            {
                countryTo = new Country
                {
                    Id = Guid.NewGuid(),
                    Name = toName
                };
                Context.Countries.Add(countryTo);
                Context.SaveChanges();
            }

            if (Context.MigrationDatas.Any(x => x.From.Id == countryFrom.Id && x.To.Id == countryTo.Id &&
                                                x.Date.Year == date.Year && (int) x.Value == (int) value))
            {
                return;
            }

            var migationData = new MigrationData
            {
                Id = Guid.NewGuid(),
                From = countryFrom,
                To = countryTo,
                Value = value,
                Date = date
            };

            Context.MigrationDatas.Add(migationData);
            Context.SaveChanges();
        }
        
        public IList<MigrationData> GetAllDataByFromCountry(string countryName)
        {
            var countries = Context.Countries.ToList();
            return Context.MigrationDatas
                .Where(x => x.From.Name.Contains(countryName))
                .ToList()
                .DistinctBy(x => new { x.To, x.Date })
                .ToList();
        }

        public IList<MigrationData> GetAllDataByToCountry(string countryName)
        {
            var countries = Context.Countries.ToList();
            return Context.MigrationDatas
                .Where(x => x.To.Name.Contains(countryName))
                .ToList()
                .DistinctBy(x => new { x.From, x.Date })
                .ToList();
        }
    }
}