﻿using IbnFadlan.Data.Models.Locations;
using IbnFadlan.Data.Repository.General;
using IbnFadlan.Data.Repository.Interface;

namespace IbnFadlan.Data.Repository.Implemetation
{
    public class PlacesUnitOfWork : UnitOfWorkBase, IPlacesUnitOfWork
    {
        private GenericRepository<City> _cityRepository;
        private GenericRepository<Country> _countryRepository;
        private GenericRepository<Region> _regionRepository;

        public GenericRepository<Country> CountryRepository => _countryRepository ??
                                                               (_countryRepository =
                                                                   new GenericRepository<Country>(Context));

        public GenericRepository<Region> RegionRepository => _regionRepository ??
                                                             (_regionRepository =
                                                                 new GenericRepository<Region>(Context));

        public GenericRepository<City> CityRepository => _cityRepository ??
                                                         (_cityRepository = new GenericRepository<City>(Context));
    }
}