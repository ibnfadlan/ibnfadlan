﻿using System.Data.Entity;
using IbnFadlan.Data.Models;
using IbnFadlan.Data.Models.Documents;
using IbnFadlan.Data.Models.Locations;
using IbnFadlan.Data.Models.Statistics;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IbnFadlan.Data.Repository
{
    public class DatabaseContext : IdentityDbContext
    {
        public IDbSet<PermanentResidency> PermanentResidencies { get; set; }

        public IDbSet<TravelVisa> TravelVisas { get; set; }

        public IDbSet<Airport> Airports { get; set; }

        public IDbSet<Attraction> Attractions { get; set; }

        public IDbSet<City> Citis { get; set; }

        public IDbSet<Country> Countries { get; set; }

        public IDbSet<University> Universities { get; set; }

        public IDbSet<Region> Regions { get; set; }

        public IDbSet<MigrationData> MigrationDatas { get; set; }

        public IDbSet<Tag> Tags { get; set; }

        public IDbSet<ApplicationUser> ApplicationUsers { get; set; }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }
    }
}