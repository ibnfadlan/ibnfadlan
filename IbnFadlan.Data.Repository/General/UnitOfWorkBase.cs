﻿using System;

namespace IbnFadlan.Data.Repository.General
{
    public class UnitOfWorkBase : IDisposable
    {
        private bool _disposed;
        protected DatabaseContext Context;

        public UnitOfWorkBase()
        {
            Context = new DatabaseContext();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            Context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing)
                    Context.Dispose();
            _disposed = true;
        }
    }
}