﻿using System.Collections.Generic;
using IbnFadlan.Data.Models.Locations;
using IbnFadlan.Data.Models.Statistics;
using IbnFadlan.Data.Repository.General;

namespace IbnFadlan.Data.Repository.Interface
{
    public interface IMigationDataUnitOfWork
    {
        GenericRepository<Country> CountryRepository { get; }
        GenericRepository<MigrationData> MigrationDataRepository { get; }
        void SaveMigration(Dictionary<string, object> element);
        IList<MigrationData> GetAllDataByFromCountry(string countryName);
        IList<MigrationData> GetAllDataByToCountry(string countryName);
    }
}