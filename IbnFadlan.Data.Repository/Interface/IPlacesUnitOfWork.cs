using IbnFadlan.Data.Models.Locations;
using IbnFadlan.Data.Repository.General;

namespace IbnFadlan.Data.Repository.Interface
{
    public interface IPlacesUnitOfWork
    {
        GenericRepository<Country> CountryRepository { get; }
        GenericRepository<Region> RegionRepository { get; }
        GenericRepository<City> CityRepository { get; }
    }
}