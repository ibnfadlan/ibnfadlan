﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IbnFadlan.Core.Models;

namespace IbnFadlan.Crawler.Interfaces
{
    public interface ICrawler
    {
        string Name { get; }

        Task<IList<MigrationRecord>> GetMigrationDataAsync();
    }
}