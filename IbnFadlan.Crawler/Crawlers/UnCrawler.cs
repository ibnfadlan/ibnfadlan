﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using IbnFadlan.Core.Extensions;
using IbnFadlan.Core.Models;
using IbnFadlan.Crawler.Interfaces;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Linq;

namespace IbnFadlan.Crawler.Crawlers
{
    public class UnCrawler : ICrawler
    {
        private const string Link =
            "http://www.un.org/en/development/desa/population/migration/data/estimates2/data/UN_MigrantStockByOriginAndDestination_2017.xlsx";
        
        private readonly IList<string> _countries = new List<string>
        {
            "Belarus",
            "Poland",
            "Russian Federation",
            "Ukraine",
            "Republic of Moldova",
            "Latvia",
            "Lithuania",
            "Estonia",
            "Germany",
            "Syrian Arab Republic",
            "France",
            "Switzerland"
        };

        public string Name { get; } = "UN2";

        public async Task<IList<MigrationRecord>> GetMigrationDataAsync()
        {
            var result = new List<MigrationRecord>();

            var path = Guid.NewGuid() + ".xlsx";
            var url = string.Format(Link);

            if (!File.Exists(path))
                using (var client = new WebClient())
                {
                    try
                    {
                        await client.DownloadFileTaskAsync(url, path);
                    }
                    catch (WebException)
                    {
                        //no file to process
                        if (File.Exists(path))
                            File.Delete(path);
                    }
                }
            var full = Path.GetFullPath(path);
            if (!File.Exists(full)) return null;
            using (var stream = File.OpenRead(full))
            {
                if (stream.Length == 0) return null;
                var document = new XSSFWorkbook(stream);
                var sheet = document.GetSheet("Table 1");

                ProcessSheet(sheet, 29, result);
            }

            return result
                .Where(x => _countries.Contains(x.From) || _countries.Contains(x.To))
                .DistinctBy(x => new { x.Date, x.From, x.To, x.Value })
                .ToList();
        }

        private void ProcessSheet(ISheet sheet, int firstRow, IList<MigrationRecord> list)
        {
            try
            {
                for (var i = firstRow; i <= sheet.LastRowNum; i++)
                {
                    var row = sheet.GetRow(i);
                    if (!ShouldProcess(row)) continue;
                    var year = (int)row.GetCell(0).NumericCellValue;
                    var to = row.GetCell(2).StringCellValue;
                    
                    for (var j = 9; j < row.LastCellNum; j++)
                    {
                        var toCell = sheet.GetRow(15).GetCell(j);
                        if (toCell == null)
                            continue;
                        var from = toCell.StringCellValue;
                        var cell = sheet.GetRow(i).GetCell(j);

                        if (cell.CellType != CellType.Numeric)
                        {
                            continue;
                        }
                        var numberOfPeople = (int)cell.NumericCellValue;
                        
                        var element = new MigrationRecord
                        {
                            To = to,
                            From = from,
                            Value = numberOfPeople,
                            Date = new DateTime(year, 1, 1)
                        };
                        list.Add(element);
                    }
                }
            }
            catch (Exception e)
            {
                // ignored
            }
        }

        private bool ShouldProcess(IRow row)
        {
            if (row.GetCell(0)
                    .CellStyle == null)
                return true;
            return row.GetCell(0)
                       .CellStyle.GetFont(row.Sheet.Workbook).Boldweight != (int) FontBoldWeight.Bold;
        }
    }
}